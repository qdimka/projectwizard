﻿using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.ViewModels.TreeData;
using Ninject;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ascon.ProjectWizard.Common.MVVMCommon.Command;
using Ascon.ProjectWizard.Wizard.Navigation;

namespace Ascon.ProjectWizard.Wizard.ViewModels
{
    [NavigationStep(3)]
    public class SectionViewModel : NavigationBase
    {
        private readonly string key = nameof(SectionViewModel);
        private SectionToolsViewModel _sectionTool;
        private TreeViewModel _treeViewModel;
        private ObservableCollection<ProjectSection> _sections;
        private IStateStorage _storage;
        private ProjectType _currenType = ProjectType.Cache;

        public SectionViewModel()
        {
            _storage = NinjectCommon.Kernel.Get<IStateStorage>();

            _sections = _storage.GetFromCache(key);
            
            if (!_sections.Any())
            {
                _currenType = ProjectType.Linear;
                _sections = _storage.GetTemplate(_currenType, key);
                _storage.IntoCache(key, _sections);
            }

            //SkipCommandCanExecute = () => !_storage.IsEditedMode;
            
            SkipUserAction = () =>
            {
                _storage.SkipObjects(key);
            };
            
            Messenger.RegisterObserver(key, this);
        }

        public SectionToolsViewModel SectionTool
        {
            get => _sectionTool ?? (_sectionTool = new SectionToolsViewModel(_currenType,key, _storage.IsComboBoxEnabled));
            set
            {
                _sectionTool = value;
                NotifyPropertyChanged(nameof(SectionTool));
            }
        }

        public TreeViewModel TreeViewModel
        {
            get => _treeViewModel ?? (_treeViewModel = new TreeViewModel(_sections, SectionType.SECTION, key));
            set
            {
                _treeViewModel = value;

                NotifyPropertyChanged(nameof(TreeViewModel));
            }
        }

        public override void Notify(object message)
        {
            if (message is ProjectType type)
            {
                _sections = _storage.GetTemplate(type, key);
                _storage.IntoCache(key, _sections);
                TreeViewModel = new TreeViewModel(_sections, SectionType.SECTION, key);
            }
        }
    }
}
