﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ascon.ProjectWizard.Common.MVVMCommon.Command;
using Ascon.ProjectWizard.Wizard.ViewModels.TreeData;
using System.Windows.Input;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.Navigation;
using Ascon.ProjectWizard.Wizard.Utils;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.ViewModels
{
    [NavigationStep(4)]
    public class PhaseViewModel : NavigationBase
    {
        private readonly string key = nameof(PhaseViewModel);
        private int _phasesCount; 
        private TreeViewModel _treeViewModel;
        private ICommand _plusCommand;
        private ICommand _minusCommand;
        private ICommand _changeButtonEditCommand;
        private IStateStorage _storage;

        private ObservableCollection<ProjectSection> _phases;

        public PhaseViewModel()
        {
            _storage = NinjectCommon.Kernel.Get<IStateStorage>();

            _phases = _storage.GetPhases(key);
            
            _treeViewModel = new TreeViewModel(_phases, SectionType.PHASE, key);

            _phasesCount = _treeViewModel.NodeViewModels.Count();

            SkipUserAction = () =>
            {
                _storage.SkipObjects(key);
            };
        }

        public int PhasesCount
        {
            get => _phasesCount;
            set
            {
                var tempCount = Helper.ValidateLength(value);
                
                if(Helper.CreateOrRemove(tempCount, new ProjectSection { Type = SectionType.WORK_DOCUMENTATION, PressMark = NinjectCommon.Kernel.Get<ProjectSection>().PressMark}, _treeViewModel.NodeViewModels, key))
                    _phasesCount = tempCount;

                NotifyPropertyChanged(nameof(PhasesCount));
            }
        }

        public TreeViewModel TreeViewModel
        {
            get => _treeViewModel;
            set
            {
                _treeViewModel = value;
                NotifyPropertyChanged(nameof(TreeViewModel));
            }
        }

        public ICommand PlusCommand => _plusCommand ??
                                       (_plusCommand = new DelegateCommand(() =>
                                       {
                                           PhasesCount++;
                                       }));

        public ICommand MinusCommand => _minusCommand ??
                                        (_minusCommand = new DelegateCommand(() =>
                                        {
                                            PhasesCount--;
                                        }));

        public ICommand ChangeButtonEditCommand => _changeButtonEditCommand ??
            (_changeButtonEditCommand = new DelegateCommand<string>(x =>
            {
                PhasesCount = int.Parse(x);
            }));

    }
}   
