﻿using System.Windows.Input;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Common.MVVMCommon.Command;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Navigation;
using Ninject;
using Ascon.ProjectWizard.Common.Common;

namespace Ascon.ProjectWizard.Wizard.ViewModels
{
    [NavigationStep(6)]
    public class FinishViewModel : NavigationBase
    {
        public FinishViewModel()
        {
            NextCommandVisible = false;
            
            PrevCommandVisible = true;

            SkipCommandVisible = false;
        }

        public ICommand CreateProjectCommand => new DelegateCommandAsync(async () =>
        {
            var storage = NinjectCommon.Kernel.Get<IStateStorage>();
            
            var creator = NinjectCommon.Kernel.Get<ICreator>();

            await creator.CreateAsync(storage);

            NinjectCommon.Kernel.Get<IMessageService>().Disable = true;

            Messenger.SendMessage(nameof(RootViewModel), null);
        });
    }
}