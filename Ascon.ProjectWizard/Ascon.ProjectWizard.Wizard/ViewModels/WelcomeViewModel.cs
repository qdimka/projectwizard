﻿using Ascon.ProjectWizard.Common.MVVMCommon.Command;
using System.Windows.Input;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.Navigation;
using Ascon.ProjectWizard.Wizard.Utils.Storages;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.ViewModels
{
    [NavigationStep(1)]
    public class WelcomeViewModel : NavigationBase
    {
        private ICommand _createNewCommand;

        private ICommand _changeExistingCommand;

        public WelcomeViewModel()
        {
            NextCommandVisible = false;
            PrevCommandVisible = false;
            SkipCommandVisible = false;
        }

        public ICommand CreateNewCommand => _createNewCommand ?? ( _createNewCommand = 
            new DelegateCommand(() => Navigate(this, Direction.NEXT), () => !NinjectCommon.Kernel.Get<IStateStorage>().IsEditedMode));

        public ICommand ChangeExistingCommand => _changeExistingCommand ?? ( _changeExistingCommand =
            new DelegateCommand(() => Navigate(this, Direction.NEXT), () => NinjectCommon.Kernel.Get<IStateStorage>().IsEditedMode));
    }
}
