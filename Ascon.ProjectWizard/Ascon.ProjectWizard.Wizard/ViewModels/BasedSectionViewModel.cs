﻿using Ascon.ProjectWizard.Common.MVVMCommon.Command;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.ViewModels.TreeData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Navigation;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.ViewModels
{
    [NavigationStep(5)]
    public class BasedSectionViewModel : NavigationBase
    {
        private readonly string key = nameof(BasedSectionViewModel);
        private ICommand _createBasedStage;
        private SectionToolsViewModel _sectionTool;
        private TreeViewModel _treeViewModel;
        private IStateStorage _storage;
        private ObservableCollection<ProjectSection> _sections;
        private ProjectType _currenType;

        public BasedSectionViewModel()
        {
            _storage = NinjectCommon.Kernel.Get<IStateStorage>();

            _sections = _storage.GetFromCache(key);
            
            if (!_sections.Any())
            {
                _currenType = ProjectType.Linear;
                _sections = _storage.GetTemplate(_currenType, key);
                _storage.IntoCache(key, _sections);
            }
            
            SkipUserAction = () =>
            {
                _storage.SkipObjects(key);
            };
            
            Messenger.RegisterObserver(key, this);
        }
        
        public SectionToolsViewModel SectionTool
        {
            get
            {
                return _sectionTool ?? (_sectionTool = new SectionToolsViewModel(_currenType, key, _storage.IsComboBoxEnabled));
            }
            set
            {
                _sectionTool = value;
                NotifyPropertyChanged(nameof(SectionTool));
            }
        }

        public TreeViewModel TreeViewModel
        {
            get { return _treeViewModel ?? (_treeViewModel = new TreeViewModel(_sections, SectionType.SECTION, key)); }
            set
            {
                _treeViewModel = value;

                NotifyPropertyChanged(nameof(TreeViewModel));
            }
         }

        public ICommand CreateBasedStage => _createBasedStage ??
            (_createBasedStage = new DelegateCommand(() =>
            {
                try
                {
                    _sections = _storage.GetTemplate(ProjectType.Cache, nameof(SectionViewModel)).ChangeExists();

                    _storage.IntoCache(key, _sections);
                    TreeViewModel = new TreeViewModel(_sections, SectionType.SECTION, key);
                }
                catch
                {

                }
            }));
        
        public override void Notify(object message)
        {
            if (message is ProjectType type)
            {
                _sections = _storage.GetTemplate(type, key);
                _storage.IntoCache(key, _sections);
                TreeViewModel = new TreeViewModel(_sections, SectionType.SECTION, key);
            }
        }
    }


}
