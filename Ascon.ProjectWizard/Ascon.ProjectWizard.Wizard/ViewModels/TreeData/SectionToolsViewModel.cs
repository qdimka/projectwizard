﻿using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;
using Ascon.ProjectWizard.Wizard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ascon.ProjectWizard.Wizard.ViewModels.TreeData
{
    public class SectionToolsViewModel: ViewModelBase
    {
        private ProjectType _projectType;
        private readonly string _parentKey;
        private bool _isEnabled;

        public SectionToolsViewModel(ProjectType type,string parentKey, bool isEnabled = true)
        {
            _projectType = type;
            _parentKey = parentKey;
            IsEnabled = isEnabled;
        }
        
        public ProjectType ProjectType
        {
            get => _projectType;
            set
            {
                _projectType = value;

                Messenger.SendMessage(_parentKey,_projectType);

                NotifyPropertyChanged(nameof(ProjectType));
            }
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set
            {
                _isEnabled = value;
                NotifyPropertyChanged(nameof(IsEnabled));
            }
        }
    }
}
