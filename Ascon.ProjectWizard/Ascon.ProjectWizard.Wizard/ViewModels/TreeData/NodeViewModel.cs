﻿using Ascon.ProjectWizard.Wizard.Models;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Ascon.ProjectWizard.Common.MVVMCommon.Command;
using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;
using Ascon.ProjectWizard.Wizard.Utils;

namespace Ascon.ProjectWizard.Wizard.ViewModels.TreeData
{
    public class NodeViewModel : ViewModelBase
    {
        private ObservableCollection<NodeViewModel> _sections;
        private ProjectSection _section;

        private ICommand _plusCommand;
        private ICommand _minusCommand;
        private ICommand _changeButtonEditCommand;
        private ICommand _editingModeCommand;

        private int _sectionCount;
        private bool _isEditingName;
        private ObservableCollection<ProjectSection> _objects;
        private string _storageKey;

        public NodeViewModel(ProjectSection projectSection, ObservableCollection<ProjectSection> objects, string storageKey)
        {
            _section = projectSection;

            _objects = objects;

            _storageKey = storageKey;

            _sections = new ObservableCollection<NodeViewModel>(
                objects.Where(x => x.ParentId == projectSection.Id && x.State != ProjectState.DELETE).Select(x => new NodeViewModel(x, objects, _storageKey))
            );

            _sectionCount = _sections.Count;
        }

        public ProjectState State
        {
            get { return _section.State; }
            set
            {
                _section.State = value;
                NotifyPropertyChanged(nameof(State));
            }
        }

        public string SectionName
        {
            get { return _section.Name; }
            set
            {
                _section.Name = value;
                NotifyPropertyChanged(nameof(SectionName));
            }
        }

        public ProjectSection Section
        {
            get => _section; 
            set
            {
                _section = value;
                NotifyPropertyChanged(nameof(Section));
            }
        }

        public ObservableCollection<NodeViewModel> Sections
        {
            get => _sections;
            set
            {
                _sections = value;
                NotifyPropertyChanged(nameof(Sections));
            }
        }    

        public bool IsEditingName
        {
            get { return _isEditingName; }
            set
            {
                if(!_section.ExistsInPilot)
                    _isEditingName = value;
                NotifyPropertyChanged(nameof(IsEditingName));
            }
        }        

        public ICommand PlusCommand => _plusCommand ?? (_plusCommand = new DelegateCommand(() => 
        {
            SectionCount++;
        }));

        public ICommand MinusCommand => _minusCommand ?? (_minusCommand = new DelegateCommand(() => 
        {
            SectionCount--;
        }));

        public ICommand ChangeButtonEditCommand => _changeButtonEditCommand ??
            (_changeButtonEditCommand = new DelegateCommand<string>(x =>
            {
                SectionCount = int.Parse(x); 
            }));

        public ICommand EditingModeCommand => _editingModeCommand ??
            (_editingModeCommand = new DelegateCommand<string>(x => 
            {
                IsEditingName = x.Equals("1");
            }));

        public int SectionCount
        {
            get => _sectionCount;
            set
            {
                var tempCount = Helper.ValidateLength(value);

                if (Helper.CreateOrRemove(tempCount, _section, Sections, _storageKey))
                    _sectionCount = tempCount;
    
                NotifyPropertyChanged(nameof(SectionCount));
            }
        }       
    }
  }