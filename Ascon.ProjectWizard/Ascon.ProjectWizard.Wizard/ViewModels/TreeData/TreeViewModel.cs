﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Windows.Input;
using Ascon.ProjectWizard.Common.MVVMCommon.Command;
using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;
using Ascon.ProjectWizard.Wizard.Models;

namespace Ascon.ProjectWizard.Wizard.ViewModels.TreeData
{
    public class TreeViewModel : ViewModelBase
    {
        private ObservableCollection<NodeViewModel> _nodeViewModels;

        private ObservableCollection<ProjectSection> _objects = new ObservableCollection<ProjectSection>();
        private string _storageKey;

        public TreeViewModel(ObservableCollection<ProjectSection> sections, SectionType topLevelSection, string storageKey)
        {
            _storageKey = storageKey;
            _objects = sections;
            _nodeViewModels = new ObservableCollection<NodeViewModel>(
                _objects.Where(x => x.Type == topLevelSection).Select(x => new NodeViewModel(x, _objects,_storageKey))
               );
        }
        
        public ObservableCollection<NodeViewModel> NodeViewModels
        {
            get => _nodeViewModels;
            set
            {
                _nodeViewModels = value;
                NotifyPropertyChanged(nameof(NodeViewModels));
            }
        }

        public ObservableCollection<ProjectSection> Objects
        {
            get => _objects;
            set
            {
                _objects = value;
                NotifyPropertyChanged(nameof(Objects));
            }
        }
    }
}