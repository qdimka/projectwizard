﻿using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;
using Ninject;
using System;
using System.Windows.Forms;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.Navigation;
using Ascon.ProjectWizard.Wizard.Infrastructure;

namespace Ascon.ProjectWizard.Wizard.ViewModels
{
    public class RootViewModel : ViewModelBase, IUserControlNavigator
    {
        private Action _close;
        private ViewModelBase _currentViewModel;

        public RootViewModel()   
        {
            NinjectCommon.Kernel.Rebind<IUserControlNavigator>().ToMethod(c => this).InSingletonScope();
            Messenger.RegisterObserver(nameof(RootViewModel),this);
        }

        public Action Close
        {
            get { return _close; }
            set
            {
                _close = value;
                NotifyPropertyChanged(nameof(Close));
            }
        }

        public ViewModelBase CurrentViewModel
        {
            get { return _currentViewModel ??(_currentViewModel = new WelcomeViewModel()); }
            set
            {
                _currentViewModel = value;
                NotifyPropertyChanged(nameof(CurrentViewModel));
            }
        }

        public void NavigateTo(ViewModelBase viewModel)
        {
            CurrentViewModel = viewModel ?? throw new ArgumentNullException(nameof(viewModel));
        }

        public override void Notify(object message)
        {
            Close?.Invoke();
        }
    }
}
