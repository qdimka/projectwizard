﻿using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Common.MVVMCommon.Command;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.Navigation;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.ViewModels
{
    [NavigationStep(2)]
    public class StageViewModel : NavigationBase
    {
        private ObservableCollection<ProjectSection> _stages;

        public StageViewModel()
        {
            var storage = NinjectCommon.Kernel.Get<IStateStorage>();
            Stages = storage.GetProjectStages(nameof(StageViewModel));

            NextCommandCanExecute = () => Stages.Any(x => x.IsSelected);

            SkipCommandVisible = false;

            PrevCommandVisible = false;
        }

        public ObservableCollection<ProjectSection> Stages
        {
            get => _stages;
            set
            {
                _stages = value;
                NotifyPropertyChanged(nameof(Stages));
            }
        }
    }
}
