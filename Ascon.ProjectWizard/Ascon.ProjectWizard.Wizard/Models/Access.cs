﻿using System;
using Ascon.Pilot.SDK;

namespace Ascon.ProjectWizard.Wizard.Models
{
    public class Access : IAccess
    {
        public AccessLevel AccessLevel { get; set; }
        
        public DateTime ValidThrough { get; set; }

        public bool IsInheritable { get; set; }

        public bool IsInherited { get; set; }
    }
}