﻿using System.ComponentModel;
using Ascon.ProjectWizard.Common.MVVMCommon.Converters;

namespace Ascon.ProjectWizard.Wizard.Models
{
    [TypeConverter(typeof(EnumToItemSourceConverter))]
    public enum ProjectType
    {
        [Description("Линейный объект")]
        Linear,
        [Description("Площадочный объект")]
        Area,
        [Description("Из кэша")]
        Cache
    }
}
