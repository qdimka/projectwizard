﻿using System;
using Ascon.Pilot.SDK;

namespace Ascon.ProjectWizard.Wizard.Models
{
    public class AccessRecord : IAccessRecord
    {
        public int OrgUnitId { get; set; }
        
        public IAccess Access { get; set; }
        
        public int RecordOwner { get; set; }
        
        public Guid InheritanceSource { get; set; }
    }
}