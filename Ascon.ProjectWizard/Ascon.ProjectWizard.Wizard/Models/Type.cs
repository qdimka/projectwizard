﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ascon.Pilot.SDK;
using System;

namespace Ascon.ProjectWizard.Wizard.Models
{
    [Serializable]
    public class PilotType : IType
    {
        public PilotType(IType type)
        {
            Id = type.Id;
            
            Name = type.Name;
            
            Title = type.Title;
            
            Sort = type.Sort;
            
            HasFiles = type.HasFiles;
            
            Children = type.Children;
            
            Attributes = new ReadOnlyCollection<IAttribute>(
                type.Attributes
                    .Select(x => (IAttribute)new PilotAttribute(x))
                    .ToList()
                );
            
            DisplayAttributes = type.DisplayAttributes.Select(x => new PilotAttribute(x));

            SvgIcon = type.SvgIcon;

            IsMountable = type.IsMountable;

            Kind = type.Kind;

            IsDeleted = type.IsDeleted;

            IsService = type.IsService;

            IsProject = type.IsProject;
        }
        
        public int Id { get; }
        public string Name { get; }
        public string Title { get; }
        public int Sort { get; }
        public bool HasFiles { get; }

        public ReadOnlyCollection<int> Children { get; }
        public ReadOnlyCollection<IAttribute> Attributes { get; }
        public IEnumerable<IAttribute> DisplayAttributes { get; }
        public byte[] SvgIcon { get; }
        public bool IsMountable { get; }
        public TypeKind Kind { get; }
        public bool IsDeleted { get; }
        public bool IsService { get; }
        public bool IsProject { get; }
    }

    [Serializable]
    public class PilotAttribute : IAttribute
    {
        public PilotAttribute(IAttribute attribute)
        {
            Name = attribute.Name;
            Title = attribute.Title;
            IsObligatory = attribute.IsObligatory;
            DisplaySortOrder = attribute.DisplaySortOrder;
            ShowInObjectsExplorer = attribute.ShowInObjectsExplorer;
            IsService = attribute.IsService;
            Configuration = attribute.Configuration;
            DisplayHeight = attribute.DisplayHeight;
            Type = attribute.Type;
        }
        
        public string Name { get; }
        public string Title { get; }
        public bool IsObligatory { get; }
        public int DisplaySortOrder { get; }
        public bool ShowInObjectsExplorer { get; }
        public bool IsService { get; }
        public string Configuration { get; }
        public int DisplayHeight { get; }
        public AttributeType Type { get; }
    }
}