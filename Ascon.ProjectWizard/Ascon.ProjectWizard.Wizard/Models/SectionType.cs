﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Ascon.ProjectWizard.Wizard.Models
{
    public enum SectionType
    {
        [Description("Стадия")]
        STAGE,
        [Description("Раздел")]
        SECTION,
        [Description("Подраздел")]
        UNDERSECTION,
        [Description("Этап")]
        PHASE,
        [Description("Объект")]
        OBJECT,
        [Description("Подобъект")]
        UNDEROBJECT,
        [Description("Проект")]
        PROJECT,
        [Description("Инженерные изыскания")]
        EngineeringSurvey,
        [Description("Проектная документация")]
        PR_DOCUMENTATION,
        [Description("Рабочая документация")]
        WORK_DOCUMENTATION,
        [Description("ГлавГосЭкспертиза")]
        GGE,
        [Description("Общепроектные работы")]
        OPR,
        [Description("Проектная редакция")]
        PD_REDACTION,
        [Description("Рабочая редакция")]
        RD_REDACTION,
        [Description("Часть")]
        PART,
        [Description("Неописанный тип")]
        UNDEFINED,
        [Description("Книга")]
        BOOK
    }
}
