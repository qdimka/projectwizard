﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ascon.Pilot.SDK;

namespace Ascon.ProjectWizard.Wizard.Models
{
    public class ProjectSection : ICloneable
    {
        public ProjectSection()
        {
            Id = Guid.NewGuid();
        }
        
        public Guid Id { get; set; }

        public Guid ParentId { get; set; }

        public string Name { get; set; }

        public SectionType Type { get; set; }

        public long Index { get; set; }

        public bool IsSelected { get; set; }

        public ProjectState State { get; set; }

        public bool ExistsInPilot { get; set; }

        public ICollection<IAccessRecord> Access { get; set; } = new List<IAccessRecord>();

        public IDictionary<string, object> Attributes { get; set; } = new Dictionary<string, object>();
        
        public string PressMark { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
