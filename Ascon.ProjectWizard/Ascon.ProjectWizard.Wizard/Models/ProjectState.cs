﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ascon.ProjectWizard.Wizard.Models
{
    public enum ProjectState
    {
        NEW,
        CHANGED,
        DELETE
    }
}
