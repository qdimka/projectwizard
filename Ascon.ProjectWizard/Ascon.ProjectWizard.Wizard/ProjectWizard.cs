﻿using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Menu;
using Ascon.Pilot.Theme.ColorScheme;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.ViewModels;
using Ascon.ProjectWizard.Wizard.Views;
using Ninject;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Media;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Infrastructure.Settings;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.Navigation;
using Ascon.ProjectWizard.Wizard.Utils;
using Ascon.ProjectWizard.Wizard.Utils.Storages;
using Ascon.ProjectWizard.Wizard.Utils.Templates;
using Ascon.ProjectWizard.Common.Common;
using Ascon.ProjectWizard.Wizard.Utils.Preloaders;

namespace Ascon.ProjectWizard.Wizard
{
    [Export(typeof(IMenu<ObjectsViewContext>))]
    public class ProjectWizard : IMenu<ObjectsViewContext>
    {
        private readonly string _internalCommandName = "PROJECT_WIZARD";
        private IDataObject _currentObject;
        private IObjectsRepository _repository;
        private IObjectModifier _modifier;

        private const string TypeProject = "Project";

        [ImportingConstructor]
        public ProjectWizard(IObjectsRepository repository, IObjectModifier modifier, IPilotDialogService dialogService)
        {
            var accentColor = (Color)ColorConverter.ConvertFromString(dialogService.AccentColor);
            ColorScheme.Initialize(accentColor);

            _repository = repository;
            _modifier = modifier;

            RegisterTypes();
        }

        public void Build(IMenuBuilder builder, ObjectsViewContext context)
        {
            var settings = NinjectCommon.Kernel.Get<Settings>();            
            
            _currentObject = context?
                    .SelectedObjects
                    .FirstOrDefault(x => settings.TypesForStart.Any(t => x.Type.Name == t));

            if(_currentObject != null)
            {
                builder.AddItem(_internalCommandName, 3)
                    .WithHeader("Мастер работы с проектом");
            }
        }

        public void OnMenuItemClick(string name, ObjectsViewContext context)
        {
            if (name != _internalCommandName)
                return;

            NinjectCommon.Kernel.Rebind<ProjectSection>().ToMethod(c => _currentObject.MapToProjectSection(0)).InSingletonScope();

            var isEditMode = _currentObject.Children.Any() || 
                             (_currentObject.Type.Id != _repository.GetType(TypeProject).Id);

            if (isEditMode)
            {
                InitializeEditMode();
            }
            else
            {
                InitializeCreateMode();
            }            
        }

        private void InitializeEditMode()
        {
            NinjectCommon.Kernel.Rebind<IPreloader>().To<Preloader>().InSingletonScope();
            NinjectCommon.Kernel.Get<IPreloader>().Load(_currentObject.Id,() =>
            {
                NinjectCommon.Kernel.Rebind<TemplateBaseStorage>().To<PilotTemplateStorage>().InSingletonScope();

                IStateStorage storage = NinjectCommon.Kernel.Get<PilotStateStorage>();

                storage.IsEditedMode = true;

                NinjectCommon.Kernel.Rebind<IStateStorage>().ToMethod(o => storage).InSingletonScope();

                InitializeCommon();

                NinjectCommon.Kernel.Get<RootViewModel>().CurrentViewModel = NinjectCommon.Kernel.Get<IPreloader>()?.To;

                NinjectCommon.Kernel.Get<RootView>().Show();
            });
        }

        private void InitializeCreateMode()
        {
            NinjectCommon.Kernel.Rebind<TemplateBaseStorage>().To<TemplateStorage>().InSingletonScope();

            IStateStorage storage = NinjectCommon.Kernel.Get<LocalStateStorage>();
            
            storage.IsEditedMode = false;
            
            NinjectCommon.Kernel.Rebind<IStateStorage>().ToMethod(o => storage).InSingletonScope();

            InitializeCommon();

            NinjectCommon.Kernel.Get<RootView>().Show();
        }

        private void InitializeCommon()
        {
            NinjectCommon.Kernel.Get<IMessageService>().Disable = false;

            NinjectCommon.Kernel.Rebind<INavigationService>().To<NavigationService>().InSingletonScope();

            NinjectCommon.Kernel.Rebind<ICreator>().To<Creator>().InSingletonScope();

            NinjectCommon.Kernel.Rebind<RootViewModel>().ToSelf().InSingletonScope();
        }

        private void RegisterTypes()
        {
            NinjectCommon.Kernel.Inject(this);
            
            NinjectCommon.Kernel.Bind<IObjectsRepository>().ToMethod(c => _repository).InSingletonScope();
            
            NinjectCommon.Kernel.Bind<IObjectModifier>().ToMethod(c => _modifier).InSingletonScope();
            
            NinjectCommon.Kernel.Bind<ISettingsFactory>().To<SettingsFactory>().InSingletonScope();
            
            NinjectCommon.Kernel.Bind<Settings>().ToMethod(o => NinjectCommon.Kernel.Get<ISettingsFactory>().Read()).InSingletonScope();
        }
    }
}
