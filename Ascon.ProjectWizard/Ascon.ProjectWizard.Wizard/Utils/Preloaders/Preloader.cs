﻿using Ascon.ProjectWizard.Wizard.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Common.PilotIceCommon.Observers;
using Ninject;
using Ascon.Pilot.SDK;
using Ascon.ProjectWizard.Wizard.ViewModels;

namespace Ascon.ProjectWizard.Wizard.Utils.Preloaders
{
    public class Preloader : IPreloader
    {
        private List<ProjectSection> _tree;

        private string parentType = "Project";

        private IDataObject _parent;

        private Guid _id;

        public List<ProjectSection> Tree => _tree;

        public ViewModelBase To => GetViewModel();

        public void Load(Guid id, Action onLoad)
        {
            _id = id;
            NinjectCommon.Kernel.Get<IObjectsLoader>("parent").Load(parent =>
            {
                if (parent.Any())
                {
                    _parent = parent.First();

                    NinjectCommon.Kernel.Rebind<ProjectSection>().ToMethod(o => _parent.MapToProjectSection(0));

                    NinjectCommon.Kernel.Get<IObjectsLoader>("child").Load(objects =>
                    {
                        _tree = objects.Select(x => x.MapToProjectSection(0, true)).ToList();
                        onLoad?.Invoke();
                    }, type => true,
                    _parent.Id);
                }
            },
            type => type.Id == NinjectCommon.Kernel.Get<IObjectsRepository>().GetType(parentType).Id,
            id);
        }

        private ViewModelBase GetViewModel()
        {
            var parent = _tree.GetParentStage(_id);
            if (parent == null)
                return null;

            ViewModelBase current = null;

            switch (parent.Type)
            {
                case SectionType.EngineeringSurvey:
                    current = new StageViewModel();
                    break;
                case SectionType.PR_DOCUMENTATION:
                    current = new SectionViewModel();
                    break;
                case SectionType.WORK_DOCUMENTATION:
                    current = new PhaseViewModel();
                    break;
                case SectionType.GGE:
                    current = new BasedSectionViewModel();
                    break;
                case SectionType.OPR:
                    current = new StageViewModel();
                    break;
                default:
                    current = null;
                    break;
            }

            return current;
        }
    }
}
