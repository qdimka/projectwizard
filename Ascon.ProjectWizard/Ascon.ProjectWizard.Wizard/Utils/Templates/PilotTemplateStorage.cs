﻿using Ascon.Pilot.SDK;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Common.PilotIceCommon.Observers;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Infrastructure.Settings;
using Ascon.ProjectWizard.Wizard.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ascon.ProjectWizard.Wizard.Utils.Templates
{
    public class PilotTemplateStorage : TemplateBaseStorage
    {
        private string parentType = "Project";
        private List<ProjectSection> _objects = new List<ProjectSection>();
        private IDataObject _parent;

        public PilotTemplateStorage()
        {
            NinjectCommon.Kernel.Get<IObjectsLoader>("parent").Load(parent =>
            {
                if (parent.Any())
                {
                    _parent = parent.First();

                    NinjectCommon.Kernel.Rebind<ProjectSection>().ToMethod(o => _parent.MapToProjectSection(0));

                    NinjectCommon.Kernel.Get<IObjectsLoader>("child").Load(objects =>
                    {
                        _objects = objects.Select(x => x.MapToProjectSection(0, true)).ToList();
                        Initialize();
                    },type => true,
                    _parent.Id);
                }
            }, 
            type => type.Id == NinjectCommon.Kernel.Get<IObjectsRepository>().GetType(parentType).Id,
            Id);
        }

        public override void Initialize()
        {
            if (!_objects.Any())
                throw new Exception(nameof(_objects));

            InitializeStages();
            InitializePhases();
            InitializeTemplate();

            var settings = NinjectCommon.Kernel.Get<Settings>();

            Guid linearId = Guid.Parse(settings.LinearTemplateId);

            Guid areaId = Guid.Parse(settings.AreaTemplateId);

            SetTemplateLinear(linearId);

            SetTemplateArea(areaId);
        }

        private void InitializeStages()
        {
            ProjectSections = new List<ProjectSection>()
            {
                new ProjectSection {ParentId = Id, Name = "ОПР", Type = SectionType.OPR},
                new ProjectSection {ParentId = Id, Name = "Инженерные изыскания", Type = SectionType.EngineeringSurvey},
                new ProjectSection {ParentId = Id, Name = "Проектная документация", Type = SectionType.PR_DOCUMENTATION},
                new ProjectSection {ParentId = Id, Name = "Рабочая документация", Type = SectionType.WORK_DOCUMENTATION},
                new ProjectSection {ParentId = Id, Name = "ГлавГосЭкспертиза", Type = SectionType.GGE}
            };

            //Первые стадии
            var stages = _objects.Where(x => x.ParentId == _parent.Id && x.Type != SectionType.UNDEFINED).ToList();
            if (stages.Count != ProjectSections.Count)
            {
                stages.AddRange(ProjectSections.FindAll(x => !stages.Select(o => o.Type).Contains(x.Type)));
            }
            ProjectSections = stages;
        }

        private void InitializePhases()
        {
            PhasesTemplate = FindChildren(_objects.Where(x => x.Type == SectionType.PHASE).Any() ?
                                          _objects.First(x => x.Type == SectionType.PHASE).ParentId : Guid.Empty, _objects);
        }

        private void InitializeTemplate()
        {
            Objects = _objects;
        }
    }
}
