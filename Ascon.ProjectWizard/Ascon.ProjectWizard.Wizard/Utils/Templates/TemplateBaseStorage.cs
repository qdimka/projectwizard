﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ascon.Pilot.SDK;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Common.PilotIceCommon.Observers;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Models;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.Utils.Templates
{
    public abstract class TemplateBaseStorage
    {
        public List<ProjectSection> ProjectSections { get; set; }

        public List<ProjectSection> LinearTemplate { get; set; }

        public List<ProjectSection> AreaTemplate { get; set; }

        public List<ProjectSection> PhasesTemplate { get; set; } = new List<ProjectSection>();

        public virtual List<ProjectSection> Objects { get; set; }

        public static Guid Id => NinjectCommon.Kernel.Get<ProjectSection>().Id;

        public abstract void Initialize();

        protected virtual void SetTemplateLinear(Guid templateId)
        {
            NinjectCommon.Kernel.Get<IObjectsLoader>("child").Load(objects =>
            {
                if (!objects.Any())
                    return;

                LinearTemplate = objects.Select(x => x.MapToProjectSection(0, false))
                    .Where(x => x.Type == SectionType.SECTION ||
                                x.Type == SectionType.UNDERSECTION ||
                                x.Type == SectionType.BOOK ||
                                x.Type == SectionType.PART).ToList();
            },
                (objType) => true,
                templateId);
        }

        protected virtual void SetTemplateArea(Guid templateId)
        {
            NinjectCommon.Kernel.Get<IObjectsLoader>("child").Load(objects =>
            {
                if (!objects.Any())
                    return;

                AreaTemplate = objects.Select(x => x.MapToProjectSection(0, false))
                    .Where(x => x.Type == SectionType.SECTION ||
                                x.Type == SectionType.UNDERSECTION ||
                                x.Type == SectionType.BOOK ||
                                x.Type == SectionType.PART).ToList();                
            },
                (objType) => true,
                templateId);
        }       

        public List<ProjectSection> FindChildren(Guid parent, List<ProjectSection> objects)
        {
            var temp = new List<ProjectSection>();
            var children = objects.FindAll(x => x.ParentId == parent).ToList();

            temp.AddRange(children);

            foreach (var child in children)
            {
                temp.AddRange(FindChildren(child.Id, objects));
            }

            return temp;
        }
    }
}
