﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Infrastructure.Settings;
using Ascon.ProjectWizard.Wizard.Models;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.Utils.Templates
{
    public class TemplateStorage : TemplateBaseStorage
    {
        public TemplateStorage()
        {
            Initialize();
        }

        public override void Initialize()
        {
            var settings = NinjectCommon.Kernel.Get<Settings>();

            Guid linearId = Guid.Parse(settings.LinearTemplateId);

            Guid areaId = Guid.Parse(settings.AreaTemplateId);

            SetTemplateLinear(linearId);

            SetTemplateArea(areaId);

            ProjectSections = new List<ProjectSection>()
            {
                new ProjectSection {ParentId = Id, Name = "ОПР", Type = SectionType.OPR},
                new ProjectSection {ParentId = Id, Name = "Инженерные изыскания", Type = SectionType.EngineeringSurvey},
                new ProjectSection {ParentId = Id, Name = "Проектная документация", Type = SectionType.PR_DOCUMENTATION},
                new ProjectSection {ParentId = Id, Name = "Рабочая документация", Type = SectionType.WORK_DOCUMENTATION},
                new ProjectSection {ParentId = Id, Name = "ГлавГосЭкспертиза", Type = SectionType.GGE}
            };
        }
    }
}