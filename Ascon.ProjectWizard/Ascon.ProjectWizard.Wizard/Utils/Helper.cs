﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.AccessControl;
using Ascon.Pilot.SDK;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Infrastructure.Settings;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.ViewModels.TreeData;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.Utils
{
    public static class Helper
    {
        private static readonly IObjectsRepository repository = NinjectCommon.Kernel.Get<IObjectsRepository>();

        private static List<AttributeInfo> attributesInfo;

        public static bool CreateOrRemove(int count, ProjectSection parent, ObservableCollection<NodeViewModel> nodes, string key)
        {
            if (parent == null)
                return false;

            var storage = NinjectCommon.Kernel.Get<IStateStorage>();

            int recordCount = nodes.Count();

            for (int i = 0; i < Math.Abs(count - recordCount); i++)
            {
                if (count - recordCount > 0)
                {
                    var index = i + 1;
                    var newObject = CreateObject(parent, index + recordCount);
                    if (newObject != null)
                    {
                        nodes.Add(new NodeViewModel(newObject, new ObservableCollection<ProjectSection>(), key));
                        storage.GetFromCache(key).Add(newObject);
                    }
                }
                else
                {
                    if (!nodes.Any())
                        return false;

                    var deleted = nodes.Last();

                    if (deleted.Section.ExistsInPilot)
                        return false;

                    nodes.Remove(deleted);
                    storage.GetFromCache(key).Remove(storage.GetFromCache(key).First(x => x.Id == deleted.Section.Id));
                }
            }

            return true;
        }

        private static ProjectSection CreateObject(ProjectSection parent, int position)
        {
            ProjectSection newSection;

            switch (parent.Type)
            {
                case SectionType.OBJECT:
                    newSection = ProjectSectionFactory.Create(SectionType.UNDEROBJECT, "Подобъект", position, parent, code: $"{parent?.PressMark}-{position}");
                    break;
                case SectionType.SECTION:
                    newSection = ProjectSectionFactory.Create(SectionType.UNDERSECTION, "Подраздел", position, parent);
                    break;
                case SectionType.PHASE:
                    newSection = ProjectSectionFactory.Create(SectionType.OBJECT, "Объект", position, parent, code: $"{parent?.PressMark}-{position}");
                    break;
                case SectionType.WORK_DOCUMENTATION:
                    newSection = ProjectSectionFactory.Create(SectionType.PHASE, "Этап", position, parent, code: $"{parent?.PressMark}-{position}");
                    newSection.SetRights();
                    break;
                case SectionType.UNDERSECTION:
                    newSection = ProjectSectionFactory.Create(SectionType.BOOK, "Книга", position, parent);
                    break;
                case SectionType.BOOK:
                    newSection = ProjectSectionFactory.Create(SectionType.PART, "Часть", position, parent);
                    break;
                case SectionType.PART:
                    newSection = ProjectSectionFactory.Create(SectionType.PART, "Часть", position, parent);
                    break;
                default: return null;
            }

            return newSection;
        }

        public static int ValidateLength(int count)
        {
            return count < 0 ? 0 : (count > 99 ? 99 : count);
        }

        public static List<AttributeInfo> AttributeInfoCollection()
        {
            var types = NinjectCommon.Kernel.Get<Settings>().Types;

            if (attributesInfo == null)
            {
                attributesInfo = new List<AttributeInfo>();

                attributesInfo.AddRange(
                    types
                        .Select(o => new AttributeInfo(o.WizardType, new PilotType(repository.GetType(o.Name)), o.AttributeName,
                            o.AttributeCode))
                );
            }

            return attributesInfo;
        }

    }

    public static class ProjectSectionFactory
    {
        public static ProjectSection Create(SectionType type, string name, int index, ProjectSection parent = null, bool isSelected = true, string code = null)
        {
            return new ProjectSection
            {
                Index = index,
                ParentId = parent?.Id ?? Guid.Empty,
                Name = $"{name} {index}",
                Type = type,
                IsSelected = isSelected,
                PressMark = code,
                State = ProjectState.NEW
            };
        }
    }
}