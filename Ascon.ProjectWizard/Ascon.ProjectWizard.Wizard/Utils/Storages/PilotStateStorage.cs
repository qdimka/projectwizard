﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.ViewModels;

namespace Ascon.ProjectWizard.Wizard.Utils.Storages
{
    public class PilotStateStorage : StateStorageBase
    {
        public override ObservableCollection<ProjectSection> GetTemplate(ProjectType type, string key = null)
        {
            List<ProjectSection> currentTemplate = new List<ProjectSection>();
            var objects = _templateStorage.Objects.Select(x => (ProjectSection)x.Clone()).ToList();

            IsComboBoxEnabled = false;

            if (key == nameof(SectionViewModel))
            {
                var projectDocumentation = objects.FirstOrDefault(x => x.Type == SectionType.PR_DOCUMENTATION);
                currentTemplate = _templateStorage.FindChildren(objects.Any(x => x.ParentId == projectDocumentation?.Id) ?
                                                                objects.FirstOrDefault(x => x.ParentId == projectDocumentation?.Id).Id : Guid.Empty, objects)
                    .Where(x => x.Type != SectionType.PR_DOCUMENTATION && x.Type != SectionType.PD_REDACTION)
                    .ToList();
            }
            else if (key == nameof(BasedSectionViewModel))
            {
                var gge = objects.FirstOrDefault(x => x.Type == SectionType.GGE);
                currentTemplate = _templateStorage.FindChildren(objects.Any(x => x.ParentId == gge?.Id) ?
                                                                objects.FirstOrDefault(x => x.ParentId == gge?.Id).Id  : Guid.Empty, objects)
                    .Where(x => x.Type != SectionType.GGE && x.Type != SectionType.PD_REDACTION)
                    .ToList();
            }

            if (!currentTemplate.Any())
            {
                IsComboBoxEnabled = true;
                return base.GetTemplate(type, key);
            }

            return new ObservableCollection<ProjectSection>(
                    currentTemplate.Select(x => (ProjectSection)x.Clone())
                    );
        }

        public override List<ProjectSection> GetState()
        {
            return ProcessStages();
        }

        private List<ProjectSection> ProcessStages()
        {
            List<ProjectSection> state = new List<ProjectSection>();
            var pilotObjects = _templateStorage.Objects.Select(x => (ProjectSection)x.Clone()).ToList();

            var selected = GetStages();

            //set right for stage
            selected.FirstOrDefault(x => x.Type == SectionType.EngineeringSurvey && !x.ExistsInPilot).SetRights();

            state.AddRange(selected);

            if (!selected.Any())
                return state;

            foreach (var stage in selected)
            {
                ProjectSection redaction=null;

                switch (stage.Type)
                {
                    case SectionType.PR_DOCUMENTATION:
                        redaction = Redaction(SectionType.PD_REDACTION, stage);
                        break;
                    case SectionType.WORK_DOCUMENTATION:
                        redaction = Redaction(SectionType.RD_REDACTION, stage);
                        break;
                    case SectionType.GGE:
                        redaction = Redaction(SectionType.PD_REDACTION, stage);
                        break;
                    default:continue;
                }
                
                AppendChild(stage.Type, redaction, state);
            }

            return state;
        }

        private List<ProjectSection> GetStages()
        {
            var stages = _cached.First(x => x.Key == nameof(StageViewModel)).Value;
            return stages?.Where(x => x.IsSelected).ToList() ?? new List<ProjectSection>();
        }

        private ProjectSection Redaction(SectionType type, ProjectSection parent)
        {
            var pilotObjects = _templateStorage.Objects.Select(x => (ProjectSection)x.Clone()).ToList();

            var child = pilotObjects.Where(x => x.Type == type && x.ParentId == parent.Id);

            if (child.Any())
                return child.First();

            var redaction = ProjectSectionFactory.Create(type, Settings.RedactionName, 0, parent, true);
            redaction.Name = Settings.RedactionName;

            return redaction;
        }

        private void AppendChild(SectionType parentType, ProjectSection redaction, List<ProjectSection> objects)
        {
            if (!objects.Any() || redaction == null)
                return;

            objects.Add(redaction);

            if (parentType == SectionType.PR_DOCUMENTATION)
            {
                var t = ProcessChildren(redaction, SectionType.SECTION, nameof(SectionViewModel));
                objects.AddRange(t.ChangedGuid().Where(x => x.IsSelected).ToList());
            }

            if (parentType == SectionType.WORK_DOCUMENTATION)
                objects.AddRange(ProcessChildren(redaction, SectionType.PHASE, nameof(PhaseViewModel)));

            if (parentType == SectionType.GGE)
            {
                var t = ProcessChildren(redaction, SectionType.SECTION, nameof(BasedSectionViewModel));
                objects.AddRange(t.ChangedGuid().Where(x => x.IsSelected).ToList());
            }
        }

        private List<ProjectSection> ProcessChildren(ProjectSection parent, SectionType topLevelType, string key)
        {
            if (!_cached.ContainsKey(key))
                return new List<ProjectSection>();

            var sections = _cached.First(x => x.Key == key).Value.ToList();

            if (sections == null)
                return new List<ProjectSection>();

            var topLevel = sections.Where(x => x.Type == topLevelType);

            foreach (var top in topLevel)
            {
                top.ParentId = parent.Id;
            }

            return sections;
        }

        public override void SkipObjects(string key)
        {
            switch (key)
            {
                case nameof(SectionViewModel):
                    IntoCache(key, GetTemplate(ProjectType.Linear,key));
                    break;

                case nameof(PhaseViewModel):
                    IntoCache(key, new ObservableCollection<ProjectSection>( _templateStorage.PhasesTemplate));
                    break;

                case nameof(BasedSectionViewModel):
                    IntoCache(key, GetTemplate(ProjectType.Linear, key));
                    break;

                default:
                    break;
            }
        }
    }
}
