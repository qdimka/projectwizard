﻿using System;
using System.Collections.ObjectModel;
using Ascon.ProjectWizard.Wizard.Models;
using System.Collections.Generic;
using System.Linq;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.ViewModels;

namespace Ascon.ProjectWizard.Wizard.Utils.Storages
{
    public class LocalStateStorage : StateStorageBase
    {
        public override List<ProjectSection> GetState()
        {
            return ProcessStages();
        }

        private List<ProjectSection> ProcessStages()
        {
            List<ProjectSection> state = new List<ProjectSection>();

            var stages = _cached.First(x => x.Key == nameof(StageViewModel)).Value;

            if (stages == null)
                return new List<ProjectSection>();

            var selected = stages.Where(x => x.IsSelected).ToList();

            //set right for stage
            selected.FirstOrDefault(x => x.Type == SectionType.EngineeringSurvey && !x.ExistsInPilot).SetRights();

            state.AddRange(selected);

            foreach (var stage in selected)
            {
                switch (stage.Type)
                {
                    case SectionType.PR_DOCUMENTATION:
                        var pdRedaction = ProjectSectionFactory.Create(SectionType.PD_REDACTION, Settings.RedactionName, 0,
                            stage,
                            true);
                        pdRedaction.Name = Settings.RedactionName;
                        CreateSectionsAndAppendChildren(stage.Type, pdRedaction, state);
                        break;
                    case SectionType.GGE:
                        var redaction = ProjectSectionFactory.Create(SectionType.PD_REDACTION, Settings.RedactionName, 0,
                            stage,
                            true);
                        redaction.Name = Settings.RedactionName;
                        CreateSectionsAndAppendChildren(stage.Type, redaction, state);
                        break;
                    case SectionType.WORK_DOCUMENTATION:
                        var ggeRedaction = ProjectSectionFactory.Create(SectionType.RD_REDACTION, Settings.RedactionName, 0,
                            stage,
                            true);
                        ggeRedaction.Name = Settings.RedactionName;
                        CreateSectionsAndAppendChildren(stage.Type, ggeRedaction, state);
                        break;
                    default:
                        break;
                }
            }

            return state;
        }

        private void CreateSectionsAndAppendChildren(SectionType parentType, ProjectSection newSection, List<ProjectSection> objects)
        {
            if (!objects.Any())
                return;

            objects.Add(newSection);

            if (parentType == SectionType.PR_DOCUMENTATION)
            {
                var t = ProcessChildren(newSection, SectionType.SECTION, nameof(SectionViewModel));
                objects.AddRange(t.ChangedGuid().Where(x => x.IsSelected).ToList());
            }

            if (parentType == SectionType.WORK_DOCUMENTATION)
                objects.AddRange(ProcessChildren(newSection, SectionType.PHASE, nameof(PhaseViewModel)));

            if (parentType == SectionType.GGE)
            {
                var t = ProcessChildren(newSection, SectionType.SECTION, nameof(BasedSectionViewModel));
                objects.AddRange(t.ChangedGuid().Where(x => x.IsSelected).ToList());
            }
        }

        private List<ProjectSection> ProcessChildren(ProjectSection parent, SectionType topLevelType, string key)
        {
            if (!_cached.ContainsKey(key))
                return new List<ProjectSection>();

            var sections = _cached.First(x => x.Key == key).Value.ToList();

            if (sections == null)
                return new List<ProjectSection>();

            var topLevel = sections.Where(x => x.Type == topLevelType);

            foreach (var top in topLevel)
            {
                top.ParentId = parent.Id;
            }

            return sections;
        }

        public override void SkipObjects(string key)
        {
            IntoCache(key, new ObservableCollection<ProjectSection>());
        }
    }
}