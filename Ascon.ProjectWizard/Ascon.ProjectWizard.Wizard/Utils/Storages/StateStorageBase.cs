﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Infrastructure.Settings;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.Utils.Templates;
using Ascon.ProjectWizard.Wizard.ViewModels;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.Utils.Storages
{
    public abstract class StateStorageBase : IStateStorage
    {
        /// <summary>
        /// Шаблон площадочный
        /// </summary>
        protected ObservableCollection<ProjectSection> _areaTemplate;

        /// <summary>
        /// Шаблон линейный
        /// </summary>
        protected ObservableCollection<ProjectSection> _linearTemplate;

        /// <summary>
        /// Кешируем то, что есть на вью, чтобы не менять шаблоны
        /// </summary>
        protected IDictionary<string, ObservableCollection<ProjectSection>> _cached;

        /// <summary>
        /// Все объекты
        /// </summary>
        protected List<ProjectSection> _projectSections;

        protected TemplateBaseStorage _templateStorage;
        
        private Settings _settings;

        public Settings Settings => _settings;
        
        public bool IfGgeSetupNeeded =>
            GetProjectStages(nameof(StageViewModel)).First(x => x.Type == SectionType.GGE).IsSelected;
        
        public bool IfPdSetupNeeded =>
            GetProjectStages(nameof(StageViewModel)).First(x => x.Type == SectionType.PR_DOCUMENTATION).IsSelected;
        
        public bool IfRdSetupNeeded =>
            GetProjectStages(nameof(StageViewModel)).First(x => x.Type == SectionType.WORK_DOCUMENTATION).IsSelected;

        public bool IsEditedMode { get; set; }

        public bool IsComboBoxEnabled { get; set; }
        
        public StateStorageBase()
        {
            _templateStorage = NinjectCommon.Kernel.Get<TemplateBaseStorage>();
            _settings = NinjectCommon.Kernel.Get<Settings>();
            _cached = new Dictionary<string, ObservableCollection<ProjectSection>>();
        }

        public virtual ObservableCollection<ProjectSection> GetProjectStages(string key)
        {
            if(!_cached.ContainsKey(key))
                IntoCache(key, new ObservableCollection<ProjectSection>(
                    _templateStorage.ProjectSections.Select(x => (ProjectSection)x.Clone())
                ));

            return _cached[key];
        }

        public virtual ObservableCollection<ProjectSection> GetTemplate(ProjectType type, string key = null)
        {
            IsComboBoxEnabled = true;

            List<ProjectSection> currentTemplate = new List<ProjectSection>();

            switch (type)
            {
                case ProjectType.Area:
                    currentTemplate = _templateStorage.AreaTemplate;
                    break;
                case ProjectType.Linear:
                    currentTemplate = _templateStorage.LinearTemplate;
                    break;
                case ProjectType.Cache:
                    currentTemplate = _cached[key].ToList();
                    break;
            }

            return new ObservableCollection<ProjectSection>(
                 currentTemplate.Select(x => (ProjectSection)x.Clone())
                );    
        }

        public virtual ObservableCollection<ProjectSection> GetFromCache(string key)
        {
            if (_cached.ContainsKey(key))
                return _cached[key];
            
            return new ObservableCollection<ProjectSection>();
        }

        public virtual ObservableCollection<ProjectSection> GetPhases(string key)
        {
            if (!_cached.ContainsKey(key))
                IntoCache(key, new ObservableCollection<ProjectSection>(
                    _templateStorage.PhasesTemplate.Select(x => (ProjectSection)x.Clone()))
                );

            return _cached[key];
        }

        public void IntoCache(string key, ObservableCollection<ProjectSection> sections)
        {
            if (!_cached.ContainsKey(key))
            {
                _cached.Add(key, sections);
            }
            else
            {
                _cached[key] = sections;
            }
        }

        public abstract List<ProjectSection> GetState();

        public abstract void SkipObjects(string key);
    }
}