﻿using Ascon.Pilot.SDK;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using System.ComponentModel;
using System.Reflection;
using Ascon.ProjectWizard.Wizard.Infrastructure.Settings;

namespace Ascon.ProjectWizard.Wizard.Utils
{
    public class Creator : ICreator
    {
        private IObjectModifier _modifier => NinjectCommon.Kernel.Get<IObjectModifier>();

        private IObjectsRepository _repository => NinjectCommon.Kernel.Get<IObjectsRepository>();

        private ProjectSection _parent => NinjectCommon.Kernel.Get<ProjectSection>();

        private Settings _settings => NinjectCommon.Kernel.Get<Settings>();

        public Task CreateAsync(IState state)
        {
            return Task.Factory.StartNew(() =>
            {
                var items = state?.GetState().ToList();

                if (items != null && !items.Any())
                    return;
                
                CreateRecursive(items, _parent.Id);

                try
                {
                    _modifier?.Apply();
                }
                catch
                {

                }
            });            
        }

        private void CreateRecursive(List<ProjectSection> objects, Guid parentId)
        {
            if(!objects.Any())
                return;

            var children = objects.FindAll(o => o.ParentId == parentId);

            foreach (var child in children)
            {
                if (!child.ExistsInPilot)
                    CreateObject(child);
                
                CreateRecursive(objects, child.Id);
            }
        }

        private void CreateObject(ProjectSection obj)
        {
            var type = Helper.AttributeInfoCollection()
                .FirstOrDefault(x => x.Type == obj.Type)?.PilotType;

            if (type == null)
                throw new ArgumentNullException("type");

            var builder = _modifier.CreateById(obj.Id, obj.ParentId, _repository.GetType(type.Name));

            foreach (var access in obj.Access)
            {
                builder.SetAccessRights(access.OrgUnitId, access.Access.AccessLevel, access.Access.ValidThrough, access.Access.IsInheritable);
            }

            CopyAttributes(builder, obj);

            SetAttribute(builder, obj, type);
        }

        private void SetAttribute(IObjectBuilder builder, ProjectSection obj, IType type)
        {
            try
            {
                var nameKey = _settings.Types.FirstOrDefault(x => x.Name == type.Name)?.AttributeName ?? "";
                var codeKey = _settings.Types.FirstOrDefault(x => x.Name == type.Name)?.AttributeCode ?? "";

                if (builder.DataObject.Attributes == null)
                {
                    builder.SetAttribute(nameKey, obj.Name);
                    builder.SetAttribute(codeKey, obj.PressMark);
                }
                else
                {

                    if (builder.DataObject.Attributes.Any(x => x.Key == nameKey))
                        builder.SetAttribute(nameKey, obj.Name);

                    if (builder.DataObject.Attributes.Any(x => x.Key == codeKey))
                        builder.SetAttribute(codeKey, obj.PressMark);
                }
            }
            catch (Exception e)
            {

            }
        }

        private void CopyAttributes(IObjectBuilder builder, ProjectSection obj)
        {
            if (obj.Attributes == null || !obj.Attributes.Any())
                return;

            foreach (var pair in obj.Attributes)
            {
                builder.SetAttribute(pair.Key,pair.Value?.ToString());
            }
        }
    }
}
