﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Ascon.Pilot.SDK;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Models;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.Utils
{
    public class AttributeInfo
    {       
        public AttributeInfo(SectionType type, IType pilotType, string nameAttribute = "name", string pressMarkAttribute = "code")
        {
            TypeName = pilotType.Name ?? throw new ArgumentNullException(nameof(pilotType));
            NameAttribute = nameAttribute;
            PressMarkAttribute = pressMarkAttribute;
            Type = type;
            PilotType = pilotType;
        }
        
        public string TypeName { get; set; }

        public SectionType Type { get; set; }

        public string NameAttribute { get; set; }

        public string PressMarkAttribute { get; set; }

        public IType PilotType { get; set; }
    }
}