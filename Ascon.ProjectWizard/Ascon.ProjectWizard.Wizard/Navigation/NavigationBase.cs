﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Common.MVVMCommon.Command;
using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ninject;
using System.Threading.Tasks;

namespace Ascon.ProjectWizard.Wizard.Navigation
{
    public abstract class NavigationBase : ViewModelBase
    {
        #region NextCommand
        
        private ICommand _prevCommand;
        
        private bool _prevCommandVisible = true;
        
        private Func<bool> _prevCommandCanExecute;
        
        private Action _prevUserAction;
        
        public ICommand PrevCommand
        {
            get
            {
                return _prevCommand ??
                       (_prevCommand = new DelegateCommandAsync(() =>
                           {
                               return Task.Factory.StartNew(() =>
                               {
                                   PrevUserAction?.Invoke();
                                   Navigate(_storage, this, Direction.PREV);
                               });
                           } ,
                           PrevCommandCanExecute));
            }

            set => _prevCommand = value;
        }
        
        public Func<bool> PrevCommandCanExecute
        {
            get => _prevCommandCanExecute ?? (_prevCommandCanExecute = () => true);
            set
            {
                _prevCommandCanExecute = value;
                NotifyPropertyChanged(nameof(PrevCommandCanExecute));
            }
        }
        
        public bool PrevCommandVisible
        {
            get => _prevCommandVisible;
            set
            {
                _prevCommandVisible = value;
                NotifyPropertyChanged(nameof(PrevCommandVisible));
            }
        }

        public Action NextUserAction
        {
            get => _nextUserAction;
            set
            {
                _nextUserAction = value;
                NotifyPropertyChanged(nameof(NextUserAction));
            }
        }

        #endregion
        
        #region PrevCommand
        
        private ICommand _nextCommand;
        
        private bool _nextCommandVisible = true;
        
        private Func<bool> _nextCommandCanExecute;
        
        private Action _nextUserAction;
        
        public ICommand NextCommand
        {
            get
            {
                return _nextCommand ??
                       (_nextCommand = new DelegateCommandAsync(() =>
                       {
                           return Task.Factory.StartNew(() =>
                           {
                               NextUserAction?.Invoke();
                               Navigate(_storage, this, Direction.NEXT);
                           });
                           },
                           NextCommandCanExecute));
            }

            set => _nextCommand = value;
        }
        
        public Func<bool> NextCommandCanExecute
        {
            get => _nextCommandCanExecute ?? (_nextCommandCanExecute = () => true);
            set
            {
                _nextCommandCanExecute = value;
                NotifyPropertyChanged(nameof(NextCommandCanExecute));
            }
        }
        
        public bool NextCommandVisible
        {
            get => _nextCommandVisible;
            set
            {
                _nextCommandVisible = value;
                NotifyPropertyChanged(nameof(NextCommandVisible));
            }
        }
        
        public Action PrevUserAction
        {
            get => _prevUserAction;
            set
            {
                _prevUserAction = value;
                NotifyPropertyChanged(nameof(PrevUserAction));
            }
        }
        
        #endregion
        
        #region SkipCommand
        
        private ICommand _skipCommand;
        
        private bool _skipCommandVisible = true;
        
        private Func<bool> _skipCommandCanExecute;
        
        private Action _skipUserAction;
        
        public ICommand SkipCommand
        {
            get
            {
                return _skipCommand ??
                       (_skipCommand = new DelegateCommandAsync(() =>
                       {
                           return Task.Factory.StartNew(() =>
                           {
                               SkipUserAction?.Invoke();
                               Navigate(_storage, this, Direction.NEXT);
                           });
                           },
                           SkipCommandCanExecute));
            }

            set => _skipCommand = value;
        }
        
        public Func<bool> SkipCommandCanExecute
        {
            get => _skipCommandCanExecute ?? (_skipCommandCanExecute = () => true);
            set
            {
                _skipCommandCanExecute = value;
                NotifyPropertyChanged(nameof(SkipCommandCanExecute));
            }
        }
        
        public bool SkipCommandVisible
        {
            get => _skipCommandVisible;
            set
            {
                _skipCommandVisible = value;
                NotifyPropertyChanged(nameof(SkipCommandVisible));
            }
        }
        
        public Action SkipUserAction
        {
            get => _skipUserAction;
            set
            {
                _skipUserAction = value;
                NotifyPropertyChanged(nameof(SkipUserAction));
            }
        }
        
        #endregion
        
        private readonly INavigationService _navigationService;
        
        private readonly IStateStorage _storage;

        protected NavigationBase() : base()
        {
            _navigationService = NinjectCommon.Kernel.Get<INavigationService>();
            _storage = NinjectCommon.Kernel.Get<IStateStorage>();
        }
        
        public void Navigate(object from, Direction direction)
        {
            _navigationService.Navigate(from, direction);
        }

        public void Navigate(int? step)
        {
            _navigationService.Navigate(step);
        }

        public void Navigate(IStateStorage storage, object from, Direction direction)
        {
            _navigationService.Navigate(storage, from, direction);
        }

        public ViewModelBase GetCurrentViewModel()
        {
            return _navigationService.GetCurrent();
        }
    }

    public enum Direction
    {
        PREV,
        NEXT,
        SKIP
    }
}
