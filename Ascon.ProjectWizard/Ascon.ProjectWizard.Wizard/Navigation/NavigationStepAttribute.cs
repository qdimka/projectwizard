﻿using System;

namespace Ascon.ProjectWizard.Wizard.Navigation
{
    [AttributeUsage(AttributeTargets.Class)]
    public class NavigationStepAttribute : Attribute
    {
        public int Step { get; set; }

        public NavigationStepAttribute(int step)
        {
            Step = step;
        }
    }
}