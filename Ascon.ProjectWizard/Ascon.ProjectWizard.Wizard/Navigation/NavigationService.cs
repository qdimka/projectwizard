﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ascon.Pilot.SDK;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Models;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.Navigation
{
    public class NavigationService : INavigationService
    {
        private readonly ProjectSection _selected;
        private List<Type> _typesOfAssembly;
        private List<Type> _navigationUnits;

        public NavigationService(ProjectSection selected)
        {
            _selected = selected;
        }
        
        public void Navigate(object from, Direction direction)
        {
            var type = from.GetType();

            if (!Attribute.IsDefined(type, typeof(NavigationStepAttribute))) 
                throw new ArgumentException("Navigation step not exists");
            
            var step = GetAttributeValue(type);

            switch (direction)
            {
                case Direction.SKIP:
                case Direction.NEXT:
                    step++;
                    Navigate(step);
                    break;
                case Direction.PREV:
                    step--;
                    Navigate(step);
                    break;
            }
        }

        public void Navigate(int? step)
        {
            var derivedTypes = GetAssemblyTypes();

            if (!derivedTypes.Any())
                return;

            var navigationBaseTypes = GetTypesWithAttribute(derivedTypes);

            var viewModel = navigationBaseTypes.FirstOrDefault(x => GetAttributeValue(x) == step);

            if (viewModel != null)
                NinjectCommon.Kernel.Get<IUserControlNavigator>()
                    .NavigateTo((ViewModelBase)Activator.CreateInstance(viewModel));
        }

        private List<Type> GetTypesWithAttribute(List<Type> derivedTypes)
        {
            return _navigationUnits ?? (_navigationUnits = derivedTypes
                .Where(t => t.GetCustomAttributes(typeof(NavigationStepAttribute), false).Any()).ToList());
        }

        private List<Type> GetAssemblyTypes()
        {
            return _typesOfAssembly??(_typesOfAssembly = Assembly.GetAssembly(typeof(NavigationBase)).GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(NavigationBase))).ToList());
        }

        public void Navigate(IStateStorage storage, object from, Direction direction)
        {
            if (storage.IfPdSetupNeeded && storage.IfRdSetupNeeded && storage.IfGgeSetupNeeded)
            {   
                Navigate(from,direction);
                return;
            }

            var step = GetAttributeValue(from.GetType());

            if (direction == Direction.NEXT)
            {
                if (storage.IfPdSetupNeeded && step < 3)
                {
                    Navigate(3);
                    return;
                }

                if (storage.IfRdSetupNeeded && step < 4)
                {
                    Navigate(4);
                    return;
                }

                if (storage.IfGgeSetupNeeded && step < 5)
                {
                    Navigate(5);
                    return;
                }

                Navigate(6);
            }
            
            if (direction == Direction.PREV)
            {
                if (storage.IfGgeSetupNeeded && step > 5)
                {
                    Navigate(5);
                    return;
                }
                
                if (storage.IfRdSetupNeeded && step > 4)
                {
                    Navigate(4);
                    return;
                }
                
                if (storage.IfPdSetupNeeded && step > 3)
                {
                    Navigate(3);
                    return;
                }

                Navigate(2);
            }
        }

        public void Navigate(SectionType selectedObjectType, object to)
        {
            
        }

        public ViewModelBase GetCurrent()
        {
            throw new NotImplementedException();
        }

        private int? GetAttributeValue(Type type)
        {
            return (type.GetCustomAttributes(typeof(NavigationStepAttribute), false).First()
                as NavigationStepAttribute)?.Step;
        }
    }
}