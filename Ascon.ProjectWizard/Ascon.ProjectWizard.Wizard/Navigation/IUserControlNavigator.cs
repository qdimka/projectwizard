﻿using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;

namespace Ascon.ProjectWizard.Wizard.Navigation
{
    public interface IUserControlNavigator
    {
        void NavigateTo(ViewModelBase viewModel);
    }
}
