﻿using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;
using Ascon.ProjectWizard.Wizard.Infrastructure;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.ViewModels;

namespace Ascon.ProjectWizard.Wizard.Navigation
{
    public interface INavigationService
    {
        void Navigate(object from, Direction direction);

        void Navigate(int? step);

        void Navigate(IStateStorage storage, object from, Direction direction);
        
        void Navigate(SectionType selectedObjectType, object to);
        
        ViewModelBase GetCurrent();
    }
}