﻿using System;
using Ascon.ProjectWizard.Wizard.Models;

namespace Ascon.ProjectWizard.Wizard.Navigation
{
    [AttributeUsage(AttributeTargets.Class)]
    public class NavigationTypeAttribute : Attribute
    {
        public NavigationTypeAttribute(params SectionType[] types)
        {
            Types = types;
        }

        public SectionType[] Types { get; set; }
    }
}