﻿using Ascon.ProjectWizard.Wizard.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ascon.ProjectWizard.Common.Common;
using Ascon.ProjectWizard.Common.DICommon;

namespace Ascon.ProjectWizard.Wizard.Views
{
    /// <summary>
    /// Interaction logic for RootView.xaml
    /// </summary>
    public partial class RootView : Window
    {
        public RootView()
        {
            InitializeComponent();
        }

        [Inject]
        public RootViewModel RootViewModel
        {
            get { return this.DataContext as RootViewModel; }
            set
            {
                this.DataContext = value;
                if (RootViewModel.Close == null)
                    RootViewModel.Close = this.Close;
            }
        }

        private void RootView_OnClosing(object sender, CancelEventArgs e)
        {
            if (!NinjectCommon.Kernel.Get<IMessageService>().NotifyUser("Вы уверены?", true))
                e.Cancel = true;
        }
    }
}
