﻿using Ascon.ProjectWizard.Wizard.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Ascon.ProjectWizard.Wizard.Infrastructure
{
    public static class TemplateObjectExtensions
    {
        public static List<ProjectSection> ChangedGuid(this List<ProjectSection> source)
        {
            var sections = source.Where(x => x.Type == SectionType.SECTION);
            if (!sections.Any())
                return source;

            foreach (var section in sections)
            {
                ChangeGuid(section.ParentId, section.ParentId, source);
            }

            return source;
        }

        private static void ChangeGuid(Guid parentId, Guid parentNewId, List<ProjectSection> objects)
        {
            var children = objects.FindAll(x => x.ParentId == parentId);

            foreach (var child in children)
            {
                Guid oldId = child.Id;
                if (!child.ExistsInPilot)
                    child.Id = Guid.NewGuid();
                child.ParentId = parentNewId;
                ChangeGuid(oldId, child.Id, objects);
            }
        }

        public static ObservableCollection<ProjectSection> ChangeExists(this ObservableCollection<ProjectSection> source)
        {
            if (source.Any())
            {
                foreach (var item in source)
                {
                    item.ExistsInPilot = false;
                }
            }
            return source;
        }
    }
}
