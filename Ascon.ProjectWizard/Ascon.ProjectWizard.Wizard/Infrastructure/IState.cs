﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Ascon.ProjectWizard.Wizard.Models;

namespace Ascon.ProjectWizard.Wizard.Infrastructure
{
    public interface IState
    {
        List<ProjectSection> GetState();
    }
}