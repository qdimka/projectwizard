﻿namespace Ascon.ProjectWizard.Wizard.Infrastructure.Settings
{
    public interface ISettingsFactory
    {
        /// <summary>
        /// Create new instance of Settings
        /// </summary>
        /// <returns></returns>
        Settings Create();

        /// <summary>
        /// Read settings from file
        /// </summary>
        /// <returns></returns>
        Settings Read();

        /// <summary>
        /// Save settings to file
        /// </summary>
        /// <returns></returns>
        void Save(Settings settings);
    }
}
