﻿using System.IO;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace Ascon.ProjectWizard.Wizard.Infrastructure.Settings
{
    class SettingsFactory : ISettingsFactory
    {
        private readonly string _settingsPath =$@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}/settings.json";

        public Settings Create()
        {
            return new Settings();
        }

        public Settings Read()
        {
            Settings settings;
            
            if (!File.Exists(_settingsPath))
            {
                using (var file = File.Create(_settingsPath));
                settings = Create();
                Save(Create());

                return settings;
            }
            
            settings = JObject.Parse(
                File.ReadAllText(_settingsPath)
            ).ToObject<Settings>();

            return settings;
        }

        public void Save(Settings settings)
        {
            try
            {
                File.WriteAllText(_settingsPath, JObject.FromObject(settings).ToString());
            }
            catch
            {
                
            }
        }
    }
}
