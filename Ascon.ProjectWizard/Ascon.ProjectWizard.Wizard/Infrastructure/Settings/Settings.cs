﻿using System.Collections.Generic;
using System.Windows.Documents;
using Ascon.ProjectWizard.Wizard.Models;
using Newtonsoft.Json;

namespace Ascon.ProjectWizard.Wizard.Infrastructure.Settings
{
    public class Settings
    {
        /// <summary>
        /// Типы на которых запускается плагин
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<string> TypesForStart { get; set; } = new List<string>
        {
            "Project",
            "Proektnaya_dokumentaciya",
            "Rabochaya_dokumentaciya",
            "GGE",
            "OPR",
            "RedPD",
            "RedRD"
        };

        /// <summary>
        /// Список груп пользователей, кому дать права на инженерные изыскания
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<string> EngineeringSurveyRights { get; set; } = new List<string>
        {
            "СПП Архитектуры"
        };

        /// <summary>
        /// Список груп пользователей, кому дать права на РД, Разделы
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<string> OtherRight { get; set; } = new List<string>
        {
            "СПП Архитектуры"
        };

        /// <summary>
        /// Список типов дерева проекта
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public List<TypeDescription> Types { get; set; } = new List<TypeDescription>()
        {
            new TypeDescription(SectionType.PROJECT, "Project","Project_name", "Project_code"),
            new TypeDescription(SectionType.SECTION, "Section", "name", "code"),
            new TypeDescription(SectionType.OBJECT, "Obekt","Naimenovanie_obekta","SHifr_obekta"),
            new TypeDescription(SectionType.UNDEROBJECT, "Podobekt","Naimenovanie_obekta","SHifr_obekta"),
            new TypeDescription(SectionType.BOOK, "Kniga","Name", "Book_code"),
            new TypeDescription(SectionType.PART, "CHast","Name","SHifr_chasti"),
            new TypeDescription(SectionType.UNDERSECTION, "Subsection","name", "code"),
            new TypeDescription(SectionType.EngineeringSurvey, "Inzhenernye_izyskaniya", "Naimenovanie_II"),
            new TypeDescription(SectionType.PR_DOCUMENTATION, "Proektnaya_dokumentaciya","Naimenovanie_PD"),
            new TypeDescription(SectionType.WORK_DOCUMENTATION, "Rabochaya_dokumentaciya", "Naimenovanie_RD"),
            new TypeDescription(SectionType.PD_REDACTION, "RedPD", attributeName:"code"),
            new TypeDescription(SectionType.RD_REDACTION, "RedRD", attributeName:"code"),
            new TypeDescription(SectionType.GGE, "GGE","name"),
            new TypeDescription(SectionType.OPR, "OPR","name"),
            new TypeDescription(SectionType.PHASE, "Etap","name", "code")
        };  

        /// <summary>
        /// Линейный шаблон
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string LinearTemplateId { get; set; } = "f08f2c15-511e-4a5d-932a-a37e858ac39b";

        /// <summary>
        /// Площадочный шаблон
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string AreaTemplateId { get; set; } = "f08f2c15-511e-4a5d-932a-a37e858ac39b";

        /// <summary>
        /// Имя объектов редакция
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string RedactionName { get; set; } = "Р00";
    }
}
