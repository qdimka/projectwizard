﻿using System;
using Ascon.ProjectWizard.Wizard.Models;

namespace Ascon.ProjectWizard.Wizard.Infrastructure.Settings
{
    public class TypeDescription
    {
        public TypeDescription(SectionType type, string name, string attributeName = null,string attributeCode = null)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            AttributeName = attributeName;
            AttributeCode = attributeCode;
            WizardType = type;
        }

        [Newtonsoft.Json.JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public SectionType WizardType { get; set; }
        
        public string Name { get; set; }

        public string AttributeName { get; set; }

        public string AttributeCode { get; set; }
    }
}