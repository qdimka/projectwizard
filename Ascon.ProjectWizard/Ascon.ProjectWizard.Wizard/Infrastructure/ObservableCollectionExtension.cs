﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ascon.ProjectWizard.Wizard.Models;

namespace Ascon.ProjectWizard.Wizard.Infrastructure
{
    public static class ObservableCollectionExtension
    {
        private static List<ProjectSection> _children;
        
        public static ObservableCollection<ProjectSection> ChildrenRecursive(this ObservableCollection<ProjectSection> source, SectionType type)
        {
            _children = source.Where(x => x.Type == type).ToList();

            foreach (var child in _children)
            {
                GetChildren(source, child);
            }

            return new ObservableCollection<ProjectSection>(_children);
        }


        private static void GetChildren(ObservableCollection<ProjectSection> source, ProjectSection parent)
        {
            if (parent == null)
                return;

            var children = source.Where(x => x.ParentId == parent.Id).ToList();
            
            _children.AddRange(children);
            
            foreach (var child in children)
            {
                GetChildren(source, child);
            }
        }

        public static void AddRange<T>(this ObservableCollection<T> source, params T[] items)
        {
            foreach (var item in items)
            {
                source.Add(item);
            }
        }
    }
}