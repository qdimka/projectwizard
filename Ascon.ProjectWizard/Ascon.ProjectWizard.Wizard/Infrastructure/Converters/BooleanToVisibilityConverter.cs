﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Ascon.ProjectWizard.Wizard.Infrastructure.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool visibility)
            {
                return visibility ? Visibility.Visible : Visibility.Collapsed;
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility visibility)
            {
                return visibility == Visibility.Visible ? true : false;
            }

            return true;
        }
    }
}