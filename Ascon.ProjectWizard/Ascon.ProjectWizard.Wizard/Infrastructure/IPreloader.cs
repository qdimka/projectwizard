﻿using Ascon.ProjectWizard.Common.MVVMCommon.NTPC;
using Ascon.ProjectWizard.Wizard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ascon.ProjectWizard.Wizard.Infrastructure
{
    public interface IPreloader
    {
        List<ProjectSection> Tree { get; }

        ViewModelBase To { get; }

        void Load(Guid id, Action onLoad);
    }
}
