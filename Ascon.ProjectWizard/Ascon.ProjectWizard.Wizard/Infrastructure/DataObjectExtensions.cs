﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ascon.Pilot.SDK;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.Utils;
using Ninject;

namespace Ascon.ProjectWizard.Wizard.Infrastructure
{
    public static class DataObjectExtensions
    {
        private static readonly IObjectsRepository repository = NinjectCommon.Kernel.Get<IObjectsRepository>();
        
        public static ProjectSection MapToProjectSection(this IDataObject source, int index, bool exists = true)
        {
            var type = source.Type.MapToProjectType();
            var info = GetInfo(source.Type.Id);
            string code = String.Empty;
            if(info!= null && info.PressMarkAttribute != null)
                code = source.Attributes
                                    .FirstOrDefault(x => x.Key == info.PressMarkAttribute)
                                    .Value?
                                    .ToString() ?? String.Empty;
            return new ProjectSection
            {
                Id = source.Id,
                Name = source.DisplayName,
                ParentId = source.ParentId,
                Type = type,
                Index = index,
                IsSelected = exists,
                PressMark = code,
                ExistsInPilot = exists,
                State = ProjectState.NEW,
                Attributes = source.Attributes
            };
        }
        
        public static SectionType MapToProjectType(this IType source)
        {
            return GetInfo(source.Id)?.Type ?? SectionType.UNDEFINED;
        }

        private static AttributeInfo GetInfo(int typeId)
        {
            return Helper.AttributeInfoCollection()
                    .FirstOrDefault(x => x.PilotType.Id == typeId);
        }
    } 
}