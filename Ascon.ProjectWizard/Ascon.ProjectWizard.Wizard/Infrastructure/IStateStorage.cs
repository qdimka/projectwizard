﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Ascon.ProjectWizard.Wizard.Models;

namespace Ascon.ProjectWizard.Wizard.Infrastructure
{
    public interface IStateStorage : IState
    {
        ObservableCollection<ProjectSection> GetProjectStages(string key);

        ObservableCollection<ProjectSection> GetTemplate(ProjectType type, string key);

        ObservableCollection<ProjectSection> GetPhases(string key);

        ObservableCollection<ProjectSection> GetFromCache(string key);

        bool IsEditedMode { get; set; }

        bool IsComboBoxEnabled { get; set; }

        bool IfGgeSetupNeeded { get; }
        
        bool IfPdSetupNeeded { get; }
        
        bool IfRdSetupNeeded { get; }

        void IntoCache(string key, ObservableCollection<ProjectSection> sections);

        void SkipObjects(string key);
    }
}