﻿using System.Threading.Tasks;

namespace Ascon.ProjectWizard.Wizard.Infrastructure
{
    public interface ICreator
    {
        Task CreateAsync(IState state);
    }
}