﻿using System.Collections.Generic;
using System.Linq;
using Ascon.Pilot.SDK;
using Ascon.ProjectWizard.Common.DICommon;
using Ascon.ProjectWizard.Wizard.Models;
using Ascon.ProjectWizard.Wizard.Infrastructure.Settings;
using Ninject;
using System;

namespace Ascon.ProjectWizard.Wizard.Infrastructure
{
    public static class ProjectSectionExtensions
    {
        private static readonly IObjectsRepository repository = NinjectCommon.Kernel.Get<IObjectsRepository>();
        
        private static readonly Settings.Settings settings = NinjectCommon.Kernel.Get<Settings.Settings>();

        public static void SetRights(this ProjectSection source)
        {
            if (source == null)
                return;

            source.Access = new List<IAccessRecord>();

            if (source.Type == SectionType.EngineeringSurvey)
            {
                foreach (var orgUnit in settings.EngineeringSurveyRights)
                {
                    source.Access.Add(GetRecord(orgUnit));
                }
            }
            else
            {
                foreach (var orgUnit in settings.OtherRight)
                {
                    source.Access.Add(GetRecord(orgUnit));
                }
            }
        }

        private static IAccess GetAccess()
        {
            var access = new Access();
            access.AccessLevel = AccessLevel.ViewEdit | AccessLevel.Agreement;
            access.IsInheritable = true;
            access.IsInherited = false;
            access.ValidThrough = DateTime.MaxValue;
            return access;
        }

        private static IAccessRecord GetRecord(string orgUnit)
        {
            var accessRecord = new AccessRecord();
            
            var firstOrDefault = repository
                .GetOrganisationUnits()
                .FirstOrDefault(x => x.Title == orgUnit);
                
            if (firstOrDefault != null)
                accessRecord.OrgUnitId = firstOrDefault.Id;

            accessRecord.Access = GetAccess();

            return accessRecord;
        }

        public static ProjectSection GetParentStage(this List<ProjectSection> source, Guid childId)
        {
            var selected = source.FirstOrDefault(x => x.Id == childId);
            if (selected == null)
                return null;

            var parent = source.FirstOrDefault(x => x.Id == selected.ParentId);
            if (parent == null)
                return null;

            while (!IsStage(parent.Type))
            {
                parent = source.FirstOrDefault(x => x.Id == parent.ParentId);
                if (parent == null)
                    return null;
            }

            return parent;
        }

        private static bool IsStage(SectionType type)
        {
            return type == SectionType.EngineeringSurvey
                || type == SectionType.OPR
                || type == SectionType.PR_DOCUMENTATION
                || type == SectionType.WORK_DOCUMENTATION
                || type == SectionType.GGE
                || type == SectionType.PROJECT;
        }
    }
}