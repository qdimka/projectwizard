﻿using System.Windows.Media;

namespace Ascon.Pilot.SDK.WPFSample.Resources
{
    public partial class Icons
    {
        public static Icons Instance;

        public Icons()
        {
            Instance = this;
            InitializeComponent();
        }

        public ImageSource PluginIcon
        {
            get { return (ImageSource) this["PluginIcon"]; }
        }

        public ImageSource CreateIcon
        {
            get { return (ImageSource) this["CreateIcon"]; }
        }

        public ImageSource DeleteIcon
        {
            get { return (ImageSource) this["DeleteIcon"]; }
        }

        public ImageSource AddIcon
        {
            get { return (ImageSource) this["AddIcon"]; }
        }

        public ImageSource RemoveIcon
        {
            get { return (ImageSource) this["RemoveIcon"]; }
        }

        public ImageSource EditIcon
        {
            get { return (ImageSource) this["EditIcon"]; }
        }

        public ImageSource FileIcon
        {
            get { return (ImageSource) this["FileIcon"]; }
        }

        public ImageSource FolderIcon
        {
            get { return (ImageSource) this["FolderIcon"]; }
        }

        public ImageSource SaveIcon
        {
            get { return (ImageSource) this["SaveIcon"]; }
        }

        public ImageSource OpenFileIcon
        {
            get { return (ImageSource) this["OpenIcon"]; }
        }
    }
}