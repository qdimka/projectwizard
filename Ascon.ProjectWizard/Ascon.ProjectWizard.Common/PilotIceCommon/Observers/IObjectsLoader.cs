﻿using System;
using System.Collections.Generic;
using Ascon.Pilot.SDK;

namespace Ascon.ProjectWizard.Common.PilotIceCommon.Observers
{
    public interface IObjectsLoader : IObserver<IDataObject>
    {
        void Load(Action<IList<IDataObject>> onLoadedAction, Func<IType, bool> typesFilter, params Guid[] ids);
    }
}
